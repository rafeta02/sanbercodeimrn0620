//Soal No. 1
function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)

    arr.forEach((element, index) => {
        var object = {
            firstName: element[0],
            lastName: element[1],
            gender: element[2]
        }

        if (Number.isInteger(element[3])) {
            object.age = (element[3] > thisYear ?  'Invalid Birth Year' : (thisYear - element[3]));
        } else {
            object.age = 'Invalid Birth Year';
        }

        console.log((index + 1) + '. ' +object.firstName + ' ' + object.lastName +':', object);
    });
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
// Error case 
arrayToObject([]) // ""


//Soal No. 2
function shoppingTime(memberId, money) {
    var object = {
        'Sepatu Stacattu': 1500000,
        'Baju Zoro': 500000,
        'Baju H&N': 250000,
        'Sweater Uniklooh': 175000,
        'Casing Handphone': 50000
    };

    var listPurchased = [];

    if (!memberId) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    } else if (money < 50000) {
        return 'Mohon maaf, uang tidak cukup';
    } else {
        var counter = money;
        for (const key of Object.keys(object)) {
            if (counter >= object[key]) {
                listPurchased.push(key);
                counter -= object[key];
            }
        }

        return { memberId: memberId, money: money, listPurchased: listPurchased, changeMoney: counter };
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//Soal No. 3

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];

    result = [];

    arrPenumpang.forEach(element => {
        from = rute.findIndex(function (ele) { 
            return ele === element[1]; 
        });
        to = rute.findIndex(function (ele) { 
            return ele === element[2]; 
        });
    
        if (to > from) {
            var bayar = (to - from) * 2000;
        } else {
            var bayar = 0;
        }

        var object = {
            penumpang: element[0],
            naikDari: element[1],
            tujuan: element[2],
            bayar: bayar
        }

        result.push(object);
    });

    return result;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]