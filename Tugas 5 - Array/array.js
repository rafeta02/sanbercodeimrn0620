//Tugas No 1
console.log('Tugas No. 1');

function range(start, finish) {

    if (!start || !finish) {
        return -1;
    } else {
        var array = [];
        if (finish > start) {
            for(i = start; i <= finish; i++) {
                array.push(i);
            }
        } else {
            for(i = start; i >= finish; i--) {
                array.push(i);
            }
        }
        return array;
    }
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());


//Tugas No. 2
console.log('Tugas No. 2');

function rangeWithStep(start, finish, step) {

    if (!start || !finish || !step) {
        return -1;
    } else {
        var array = [];
        if (finish > start) {
            for(i = start; i <= finish; i+=step) {
                array.push(i);
            }
        } else {
            for(i = start; i >= finish; i-=step) {
                array.push(i);
            }
        }
        return array;
    }
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

// Tugas No. 3
console.log('Tugas No. 3');

function sum(start, finish, step = 1) {

    if (!start) {
        return 0;
    } else if(!finish) {
        return start;
    } else {
        var array = 0;
        if (finish > start) {
            for(i = start; i <= finish; i+=step) {
                array += i;
            }
        } else {
            for(i = start; i >= finish; i-=step) {
                array += i;
            }
        }
        return array;
    }
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Tugas No. 4
console.log('Tugas No. 4');

function dataHandling(array) {
    array.forEach(element => {
        console.log('Nomor ID: ' + element[0]);
        console.log('Nama Lengkap: ' + element[1]);
        console.log('TTL: ' + element[2] + ', ' + element[3]);
        console.log('Hobi: ' + element[4]);
        console.log(' ');
    });
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
dataHandling(input);


//Tugas No. 5
console.log('Tugas No. 5');

function balikKata(word) {
    var length = word.length;
    var newWord = '';

    for(i = length-1; i >= 0; i--) {
        newWord += word[i];
    }

    return newWord;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Tugas No. 6
console.log('Tugas No. 6');

function dataHandling2(array) {
    //splice
    array.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    array.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(array);
    //bulan
    var ttl = array[3].split('/');
    var bulan = ttl[1];
    switch(bulan) {
        case '01': bulan = "Januari"; break;
        case '02': bulan = "Februari"; break;
        case '03': bulan = "Maret"; break;
        case '04': bulan = "April"; break;
        case '05': bulan = "Mei"; break;
        case '06': bulan = "Juni"; break;
        case '07': bulan = "Juli"; break;
        case '08': bulan = "Agustus"; break;
        case '09': bulan = "September"; break;
        case '10': bulan = "Oktober"; break;
        case '11': bulan = "November"; break;
        case '12': bulan = "Desember"; break;
    }
    console.log(bulan);
    console.log(ttl.sort(function (value1, value2) { return value2 - value1 } ));
    var ttlJoin = array[3].split('/');
    var join = ttlJoin.join('-');
    console.log(join);
    //Nama
    var nama = array[1].slice(0, 15);
    console.log(nama);
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);