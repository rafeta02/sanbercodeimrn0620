//Tugas No. 1
console.log('Tugas No. 1');

console.log('LOOPING PERTAMA');
var counter1 = 2;
while(counter1 <= 20) {
    console.log(counter1 + ' - I love coding');

    counter1 += 2;
}

console.log('LOOPING KEDUA');
var counter2 = 20;
while(counter2 >= 2) {
    console.log(counter2 + ' - I will become a mobile developer');

    counter2 -= 2;
}

//Tugas No. 2
console.log('Tugas No. 2');

for (i = 1; i <= 20; i++) {
    if (i % 3 === 0 && i % 2 === 1) {
        console.log(i + ' - I Love Coding');
    } else if (i % 2 === 1) {
        console.log(i + ' - Santai');
    } else if (i % 2 === 0) {
        console.log(i + ' - Berkualitas');
    }
}

//Tugas No. 3
console.log('Tugas No. 3');

for(i = 0; i < 4; i++) {
    for(j = 0; j < 8; j++) {
        process.stdout.write('#');
    }
    process.stdout.write('\n');
}

//Tugas No. 4
console.log('Tugas No. 4');

for(i = 0; i < 7; i++) {
    for(j = 0; j <= i; j++) {
        process.stdout.write('#');
    }
    process.stdout.write('\n');
}

//Tugas No. 5
console.log('Tugas No. 5');

for(i = 0; i < 8; i++) {
    for(j = 0; j < 8; j++) {
        if (i % 2 === j % 2) {
            process.stdout.write(' ');
        } else {
            process.stdout.write('#');
        }
    }
    process.stdout.write('\n');
}
