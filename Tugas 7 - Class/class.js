//Tugas No. 1

//Release 0
class Animal {
    constructor(name) {
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }

    set name(name) {
        this._name = name;
    }

    set legs(legs) {
        this._legs = legs
    }

    set cold_blooded(cold_blooded) {
        this._cold_blooded = cold_blooded;
    }

    get name() {
        return this._name;
    }

    get legs() {
        return this._legs;
    }

    get cold_blooded() {
        return this._cold_blooded;
    }
}

console.log('Animal Class');
var sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

//Release 1
class Ape extends Animal {
    constructor(name) {
        super(name);
        this.legs = 2;
    }

    yell() {
        console.log('Auooo');
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name);
    }

    jump() {
        console.log('Hop Hop');
    }
}

console.log('Ape Class');
var sungokong = new Ape("kera sakti")
console.log(sungokong.name) // "shaun"
console.log(sungokong.legs) // 4
console.log(sungokong.cold_blooded) // false
sungokong.yell() // "Auooo"
 
console.log('Frog Class');
var kodok = new Frog("buduk")
console.log(kodok.name) // "shaun"
console.log(kodok.legs) // 4
console.log(kodok.cold_blooded) // false
kodok.jump() // "hop hop" 

//Tugas No. 2
class Clock {
    constructor(template) {
        this.template = template.template;
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }

    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
        // setTimeout(() => this.stop(), 10000); //stop otomatis setelah 10 second
    }

    stop() {
        clearInterval(this.timer);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 

