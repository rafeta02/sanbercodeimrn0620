function bandingkan(num1, num2) {
    if (!num2) {
        var num2 = 0;
    }

    if (!num1 || num1 < 0 || num2 < 0 || num1 == num2) {
        return -1;
    } else {
        return num1 > num2 ? num1 : num2;
    }
}

function balikString(word) {
    var length = word.length;
    var newWord = '';

    for(i = length-1; i >= 0; i--) {
        newWord += word[i];
    }

    return newWord;
}

function palindrome(word) {
    var length = word.length;
    var newWord = '';

    for(i = length-1; i >= 0; i--) {
        newWord += word[i];
    }

    if (word == newWord) {
        return true;
    } else {
        return false;
    }
}

console.log('Tugas No. A');
// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18

console.log('Tugas No. B');
// TEST CASES BalikString
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

console.log('Tugas No. C');
// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false