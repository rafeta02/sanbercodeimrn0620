// Soal No 1
console.log('Soal No 1');

var nama = 'John'; 
var peran = 'Penyihir'; //role : Penyihir, Guard, Werewolf

if (nama === '' && peran === '') {
    console.log('Nama Harus Diisi!');
} else if (nama !== '' && peran === '') {
    console.log('Halo '+ nama + ', Pilih Peranmu untuk memulai game!');
} else if (nama !== '' && peran !== '') {
    console.log('Selamat datang di Dunia Werewolf, ' + nama);
    if (peran === 'Penyihir') {
        console.log('Halo Penyihir '+ nama +', kamu dapat melihat siapa yang menjadi werewolf!');
    } else if (peran === 'Guard') {
        console.log('Halo Guard '+ nama +', kamu akan membantu melindungi temanmu dari serangan werewolf.');
    } else if (peran === 'Werewolf') {
        console.log('Halo Werewolf'+ nama +', kamu akan memakan mangsa setiap malam!');
    }
}


//Soal No 2
console.log('Soal No 2');

var hari = 21;
var bulan = 1;
var tahun = 1945;

switch(bulan) {
    case 1: bulan = "Januari"; break;
    case 2: bulan = "Februari"; break;
    case 3: bulan = "Maret"; break;
    case 4: bulan = "April"; break;
    case 5: bulan = "Mei"; break;
    case 6: bulan = "Juni"; break;
    case 7: bulan = "Juli"; break;
    case 8: bulan = "Agustus"; break;
    case 9: bulan = "September"; break;
    case 10: bulan = "Oktober"; break;
    case 11: bulan = "November"; break;
    case 12: bulan = "Desember"; break;
   }

console.log(hari + ' ' + bulan + ' ' + tahun);



