// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function baca(times, index) {
    if (index < books.length) {
        readBooks(times, books[index], jikan => {
            return baca(jikan, index + 1);
        });
    }
};

baca(10000, 0);