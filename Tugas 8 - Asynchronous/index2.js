var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
function baca(times, index) {
    if (index < books.length) {
        readBooksPromise(times, books[index])
        .then(function (sukses) {
            return baca(sukses, index + 1);
        })
        .catch(function (error) {
            console.log('Selesai');
        })
    }
}

baca(10000, 0);