class Score {

    subject;
    points = [];
    email;

    constructor(subject, points, email) {
        const arr = points ? [].concat(points) : []; //const es6

        this.subject = subject;
        this.points = [...this.points, ...arr]; //spread operator es6
        this.email = email;
    }

    average() {
        let sum = 0; //let es6
        this.points.forEach(element => {
            sum += element;
        });

        return sum/this.points.length;
    }

    okay() {
        return {email: this.email, subject: this.subject, points: this.points[0] };
    }
}

const biji = new Score('Math', [100, 90, 65, 75], 'rafeta02@student.uns.ac.id');
const nilai = new Score('English', 100, 'rafeta02@student.uns.ac.id');
console.log(biji.email, biji.subject, biji.average());
console.log(nilai.email, nilai.subject, nilai.average());

const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]

function viewScores(data, subject = 'quiz - 1') {
    let object = {};
    let array = [];
    data.forEach((element, index) => {
        if (index === 0) {
            element.forEach(ele => {
                object[ele] = [];
                array.push(ele);
            });
        } else {
            element.forEach((ele, no) => {
                object[array[no]].push(ele);
            });
        }
        
    });

    kelas = [];
    object.email.forEach((element, index) => {
        let a = new Score(subject, object[subject][index], element);
        kelas.push(a.okay());
    });
    console.log(kelas);
}

viewScores(data);
viewScores(data, "quiz - 2");
viewScores(data, "quiz - 3");

function recapScore(data) {
    data.forEach((element, index) => {
        if (index === 0) {
            // console.log('header', element);
        } else {
            let email;
            let nilai = [];
            element.forEach((ele, no) => {
                if (no === 0) {
                    email = ele;
                } else {
                    nilai.push(ele);
                }
            });

            let b = new Score('Semua', nilai, email);
            let predikat;
            if (b.average() > 90) {
                predikat = 'honour';
            } else if (b.average() > 80) {
                predikat = 'graduate';
            }  else if (b.average() > 70) {
                predikat = 'participant';
            } else {
                predikat = 'none';
            }
            console.log(`${index}. Email : ${b.email}`);
            console.log(`Rata-rata : ${b.average().toFixed(1)}`);
            console.log(`Predikat: ${predikat}`);
        }
    });

}

recapScore(data);