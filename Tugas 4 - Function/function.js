//Tugas No. 1
console.log('Tugas No. 1');

function teriak() {
    return '"Halo Sanbers!"';
}

console.log(teriak());

//Tugas No. 2
console.log('Tugas No. 2');

function multiply(num1, num2) {
    return num1 * num2;
}

var num1 = 15;
var num2 = 2;
var hasilKali = multiply(num1, num2);

console.log(hasilKali);

//Tugas No. 3
console.log('Tugas No. 3');

function introduce(name, age, address, hobby) {
    return 'Nama saya ' + name + ', umur saya ' + age + ' tahun, alamat saya di ' + address + ', dan saya punya hobby yaitu ' + hobby + '!';
}

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);